import { Box, Button, Typography } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import SelectLabels from '../SelectLabels'

const DetailCards = ({ img, category, name, price, handleAddtoCart, setDate, render, handleOpen, handleClose }) => {
    const navigate = useNavigate()

    const buyNow = () => {
        handleAddtoCart("buyNow")
        handleOpen()
    }


    return (
        <Box sx={{ minWidth: 300, maxWidth: 650, display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, rowGap: "36px", columnGap: "36px" }}>
            <Box sx={{ display: "flex", justifyContent: { xs: "center", md: "center", lg: "start" } }}>
                <img src={img} alt='food detail' style={{ minWidth: "250" }} />
            </Box>
            <Box sx={{ display: "flex", flexDirection: "column", alignItems: { xs: "center", md: "center", lg: "start" }, textAlign:{xs: "center", md: "center", lg: "start" } }}>
                <Typography variant="p" color="#828282" fontSize="16px">
                    {category}
                </Typography>
                <Typography variant="p" color="#333333" fontSize="24px" fontWeight="700" >
                    {name}
                </Typography>
                <Typography variant="p" color="#5B4947" fontSize="24px" fontWeight="700">
                    IDR {price?.toLocaleString("id-ID")}
                </Typography>

                <SelectLabels setDate={setDate} render={render} />

                <Box sx={{ display: "flex", columnGap: "16px" }}>
                    <Button
                        onClick={() => handleAddtoCart()}
                        sx={{ color: '#5B4947', borderRadius: "8px", display: 'block', border: "1px solid #5B4947", textDecoration: "none", width: { xs: "150px", md: "200px", lg: "250px" }, textTransform: "none" }}
                    >
                        Add to Cart
                    </Button>
                    <Button
                        onClick={() => buyNow()}
                        sx={{ color: '#5B4947', borderRadius: "8px", display: 'block', backgroundColor: "#FABC1D", width: { xs: "150px", md: "200px", lg: "250px" }, textTransform: "none" }}
                    >
                        Buy Now
                    </Button>
                </Box>
            </Box>
        </Box>
    )
}

export default DetailCards