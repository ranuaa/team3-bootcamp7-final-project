import React from "react";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';
import { useNavigate } from "react-router-dom";




const MenuCard = ({ img, category, name, price, id, id_categories }) => {
    const navigate = useNavigate()
    return (
        <Box sx={{ minWidth: 300, maxWidth: {xs:300, md:350, lg:400}, display: "flex" }} onClick={() => navigate(`/detail-class/${id}`, { state: { id, id_categories } })} >
            <Card variant="outlined" sx={{ minHeight: 210, minWidth:{xs:300, md:350, lg:400}  }}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="140"
                        image={img}
                        alt="pictures"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="p" component="div" color="#828282">
                            {category}
                        </Typography>
                        <Typography gutterBottom variant="h6" component="div" color="#5B4947" >
                            {name}
                        </Typography>
                        <Typography variant="h5" color="#FABC1D" fontWeight="bold">
                            IDR {price.toLocaleString("id-ID")}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Box>
    );
};

export default MenuCard;
