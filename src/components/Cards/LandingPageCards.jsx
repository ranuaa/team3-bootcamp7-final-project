import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import React from "react";



const LandingPageCards = ({ h1, text }) => {

    return (
        <Box sx={{ minWidth: 275, maxWidth: 350, display: "flex" }}>
            <Card variant="outlined" sx={{ minHeight: 210 }}>
                <CardContent>
                    <Typography textAlign="center" sx={{ fontSize: "48px" }} color="#FABC1D" gutterBottom>
                        {h1}
                    </Typography>
                    <Typography textAlign="center" sx={{ mb: 1.5 }} color="#5B4947">
                        {text}
                    </Typography>
                </CardContent>
            </Card>
        </Box>
    );
};

export default LandingPageCards;
