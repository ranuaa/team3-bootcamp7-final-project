import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import Switch from '@mui/material/Switch';
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import isEmail from "validator/lib/isEmail";
import { UserContext } from '../../Context/UserContext';


const UserAdminModal = ({ open, handleClose, userData, getUsers }) => {

    const baseUrls = process.env.REACT_APP_API;
    const { user } = useContext(UserContext)

    const [checked, setChecked] = useState({
        role: false,
        status: false
    });
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [showPassword, setShowPassword] = React.useState(false);
    const [isDisabled, setDisabled] = useState(false)

    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSwitch = (event) => {
        setChecked({
            ...checked,
            [event.target.name]: event.target.checked,
        });
    };

    const modalClose = () => {
        handleClose()
        setChecked({
            role: false,
            status: false
        })
        setName("")
        setEmail("")
        setPassword("")
        setShowPassword(false)
        setDisabled(false)
    }

    const handleSignUp = async (e) => {
        e.preventDefault();
        if (
            email === "" ||
            name === "" ||
            password === ""
        ) {
            toast.warn("Please fill all the form field");
        } else if (!isEmail(email)) {
            toast.warn("Please check your email");
        } else {
            toast.info("Saving Data")
            setDisabled(true)
            try {
                const newdata = {
                    username: name,
                    password: password,
                    email: email,
                    role: checked.role,
                    status: checked.status,
                }
                const response = await axios.post(`${baseUrls}User/Register`, newdata);

                if (response?.length !== 0) {
                    setDisabled(false)
                    getUsers()
                    modalClose()
                }


                console.log(newdata)
            } catch (error) {
                toast.error(error?.response?.data)
                setDisabled(false)
            }
        }
    };

    console.log(user)
    const handleEdit = async (e) => {
        e.preventDefault();
        if (!isEmail(email)) {
            toast.warn("Please check your email");
        } else {
            toast.info("Saving Data")
            setDisabled(true)
            try {
                const newDataNoPass = {
                    username: name,
                    email: email,
                    role: checked.role,
                    status: checked.status,
                }

                const response = await axios.put(
                    `${baseUrls}Admin/UpdateUser?id=${userData.id}&email=${userData.email}`,
                    newDataNoPass,
                    {
                        headers: {
                            Authorization: `Bearer ${user.token}`,
                            ContentType: "application/json",
                        },
                    }
                );

                if (response?.length !== 0) {
                    setDisabled(false)
                    getUsers()
                    modalClose()
                }
            } catch (error) {
                toast.error(error?.response?.data)
                setDisabled(false)
            }

            if (password.length !== 0) {
                try {


                    const passwordData = {
                        email: email,
                        password: password,
                        token: "string"

                    }

                    const response = await axios.post(
                        `${baseUrls}Admin/ResetPassword`,
                        passwordData,
                        {
                            headers: {
                                Authorization: `Bearer ${user.token}`,
                                ContentType: "application/json",
                            },
                        }
                    );

                    if (response?.length !== 0) {
                        setDisabled(false)
                        getUsers()
                        modalClose()
                    }

                } catch (error) {
                    toast.error(error?.response?.data)
                    setDisabled(false)
                }
            }
        }
    };

    const handleSave = (e) => {
        if (userData !== null) {
            handleEdit(e)
        } else {
            handleSignUp(e)
        }
    }

    useEffect(() => {
        if (userData !== null) {
            setChecked({
                role: userData.role,
                status: userData.status
            })
            setName(userData.username)
            setEmail(userData.email)
        } else {
            setChecked({
                role: false,
                status: false
            })
            setName('')
            setEmail("")
        }
    }, [userData]);


    return (
        <div>
            <Dialog open={open} onClose={modalClose}>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />
                <DialogTitle>{userData !== null ? "Edit User" : "Add New User"}</DialogTitle>
                <DialogContent>
                    <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">Name</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-name"
                            type="text"
                            label="Name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">Email Address</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-email"
                            type="email"
                            label="Email Address"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            type={showPassword ? "text" : "password"}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end">
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Password"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </FormControl>
                    <FormControl component="fieldset" sx={{ marginTop: '10px' }}>
                        <FormGroup aria-label="position" row >
                            <FormControlLabel
                                value="top"
                                control={<Switch color="primary"
                                    checked={checked.status}
                                    onChange={handleSwitch}
                                />}
                                label="active"
                                labelPlacement="top"
                                name="status"
                            />
                            <FormControlLabel
                                value="top"
                                control={<Switch color="primary"
                                    checked={checked.role}
                                    onChange={handleSwitch}
                                />}
                                label="Admin"
                                labelPlacement="top"
                                name="role"

                            />
                        </FormGroup>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button disabled={isDisabled} onClick={() => modalClose()}>Cancel</Button>
                    <Button disabled={isDisabled} onClick={(e) => handleSave(e)}>Save</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default UserAdminModal;
