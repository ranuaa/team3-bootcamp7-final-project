import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const LogoutAlert = ({ handleOpen, open, handleClose, logout }) => {
    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" textAlign="center">
                        Are you sure to log out?
                    </Typography>
                    <Box sx={{ display: "flex", justifyContent: 'space-between', alignItems: 'center', marginTop: '25px' }} >
                        <Button sx={{ border: "1px solid #5B4947" }} onClick={() => handleClose()} >Cancel</Button>
                        <Button variant="outlined" sx={{ backgroundColor: "red", color: 'white' }} onClick={logout} >Logout</Button>
                    </Box>
                </Box>
            </Modal>
        </div>
    );
};

export default LogoutAlert;
