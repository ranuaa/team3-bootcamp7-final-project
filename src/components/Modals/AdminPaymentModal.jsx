import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import Switch from '@mui/material/Switch';
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import isEmail from "validator/lib/isEmail";
import { UserContext } from '../../Context/UserContext';

const AdminPaymentModal = ({ open, handleClose, paymentData, getPayment }) => {



    const baseUrls = process.env.REACT_APP_API;
    const { user } = useContext(UserContext)


    const [uploadFile, setUploadFile] = useState("");
    const [checked, setChecked] = useState({
        status: false
    });
    const [name, setName] = useState("")
    const [logo, setLogo] = useState("")
    const [isDisabled, setDisabled] = useState(false)
    const handleSwitch = (event) => {
        setChecked({
            ...checked,
            [event.target.name]: event.target.checked,
        });
    };


    const uploadImage = () => {
        return new Promise(function (resolve, reject) {
            const data = new FormData();
            data.append("file", uploadFile);
            data.append("upload_preset", "dsoups");
            data.append("cloud_name", "dws0flefe");
            fetch("https://api.cloudinary.com/v1_1/dws0flefe/image/upload", {
                method: "post",
                body: data,
            })
                .then((resp) => resp.json())
                .then((data) => {
                    resolve(data.url);
                })
                .catch((err) => reject(err));
        });
    };

    const modalClose = () => {
        handleClose()
        setChecked({
            role: false,
            status: false
        })
        setName("")
        setLogo("")
        setUploadFile("")
        setDisabled(false)
    }

    const handleCreate = async (e) => {
        e.preventDefault();
        if (
            name === "" || uploadFile === ""
        ) {
            toast.warn("Please fill all the form field");
        } else {
            toast.info("Saving Data")
            setDisabled(true)
            try {
                const newdata = {
                    name: name,
                    logo: await uploadImage(),
                    status: checked.status,
                }
                const response = await axios.post(`${baseUrls}Admin/CreatePayment`, newdata,
                    {
                        headers: {
                            Authorization: `Bearer ${user.token}`,
                            ContentType: "application/json",
                        },
                    }
                );

                if (response?.length !== 0) {
                    setDisabled(false)
                    getPayment()
                    modalClose()
                }
                console.log(newdata)
            } catch (error) {
                toast.error(error?.response?.data)
                setDisabled(false)
            }
        }
    };

    const handleEdit = async (e) => {
        e.preventDefault();

        toast.info("Saving Data")
        setDisabled(true)
        try {
            if (uploadFile === "") {
                const response = await axios.put(
                    `${baseUrls}Admin/EditPayment?id=${paymentData.id}`,
                    {
                        name: name,
                        logo: paymentData.logo,
                        status: checked.status,
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${user.token}`,
                            ContentType: "application/json",
                        },
                    }
                );

                if (response?.length !== 0) {
                    setDisabled(false)
                    getPayment()
                    modalClose()
                }
            } else {
                const response = await axios.put(
                    `${baseUrls}Admin/EditPayment?id=${paymentData.id}`,
                    {
                        name: name,
                        logo: await uploadImage(),
                        status: checked.status,
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${user.token}`,
                            ContentType: "application/json",
                        },
                    }
                );
                console.log("Inside", uploadFile)
                if (response?.length !== 0) {
                    setDisabled(false)
                    getPayment()
                    modalClose()
                }
            }


        } catch (error) {
            toast.error(error?.response?.data)
            setDisabled(false)
        }
    };

    console.log(uploadFile)

    const handleSave = (e) => {
        if (paymentData !== null) {
            handleEdit(e)
        } else {
            handleCreate(e)
        }
    }

    useEffect(() => {
        if (paymentData !== null) {
            setChecked({
                status: paymentData.status
            })
            setName(paymentData.name)
        } else {
            setChecked({
                status: false
            })
            setName('')
        }
    }, [paymentData]);
    return (
        <div>
            <Dialog open={open} onClose={modalClose}>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />
                <DialogTitle>{paymentData !== null ? "Edit Payment" : "Add New Payment"}</DialogTitle>
                <DialogContent>
                    <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">Name</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-name"
                            type="text"
                            label="Name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                        <OutlinedInput
                            id="outlined-adornment-email"
                            type="file"
                            label="Image"
                            onChange={(event) => { setUploadFile(event.target.files[0]) }}
                        />
                    </FormControl>
                    <FormControl component="fieldset" sx={{ marginTop: '10px' }}>
                        <FormGroup aria-label="position" row >
                            <FormControlLabel
                                value="top"
                                control={<Switch color="primary"
                                    checked={checked.status}
                                    onChange={handleSwitch}
                                />}
                                label="active"
                                labelPlacement="top"
                                name="status"
                            />
                        </FormGroup>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button disabled={isDisabled} onClick={() => modalClose()}>Cancel</Button>
                    <Button disabled={isDisabled} onClick={(e) => handleSave(e)}>Save</Button>
                </DialogActions>
            </Dialog>
        </div >
    );
};

export default AdminPaymentModal;
