import { Button } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React from "react";


const AdminTables = ({ datas, handleTogleStatus, handleOpenEdit, header }) => {

    return (
        <>
            <TableContainer sx={{ marginBottom: 20 }}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <TableCell>No</TableCell>
                            {header?.map((head, index) => {
                                return (
                                    <TableCell align="center" key={index}>{head}</TableCell>
                                )
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {datas.sort((a, b) => a.role - b.role).sort((a, b) => a.status - b.status).sort().map((data, index) => (
                            <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                    {index + 1}
                                </TableCell>
                                <TableCell align="center">{data.username ? data.username : <img src={data.logo} style={{ maxWidth: "40px", maxHeight: "40px" }} />}</TableCell>
                                <TableCell align="center">{data.email ? data.email : data.name}</TableCell>
                                {header?.includes("Role") && <TableCell align="center">
                                    {data.role ? "Admin" : "Basic"}
                                </TableCell>}
                                <TableCell align="center">
                                    <Button
                                        variant={data.status ? "contained" : "outlined"}
                                        onClick={() => handleTogleStatus(data.id, data.email)}
                                    >
                                        {data.status ? "Active" : "Inactive"}
                                    </Button>
                                </TableCell>
                                <TableCell align="center">
                                    <Button variant="outlined" onClick={() => handleOpenEdit(data)}>
                                        Edit
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
};

export default AdminTables;
