import { Box, Typography } from '@mui/material';
import React, { useState } from 'react'
import CallIcon from '@mui/icons-material/Call';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';
import axios from 'axios';
import { useEffect } from 'react';

const boxIconStyle = {
    display: "flex",
    color: "#FFFFFF",
    width: "45px",
    height: "45px",
    backgroundColor: "#FABC1D",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%"
};

const flexContainer= {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between"
};
  
const flexItem = {
    width: "48%",
    margin: "10px 0px"
  }

const BannerBottom = () => {
    const [dataCategory, setDataCategory] = useState([]);
    const getCategory = async () => {
        try {
          const response = await axios.get(
            `${process.env.REACT_APP_API}User/GetAllCategories`
          );
          const data = response.data;
          setDataCategory(data);
        } catch (error) {
          console.log(error);
        }
      };
    useEffect(() => {
        getCategory();
      }, []);
    return (

        <Box sx={{
            display: "flex", justifyContent: "space-around", alignItems: "top",
            backgroundColor: "#5B4947",
            padding: "24px 64px",
            rowGap: "16px",
            columnGap: "64px",
            marginTop: "96px",
            flexDirection: { md: 'row', sm: 'column', xs: 'column' }
        }} >
            <Box sx={{ flex: "2" }}>
                <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                    About Us
                </Typography>
                <Typography textAlign="justify" mt={2} color="#FFFFFF" fontSize="14px" lineHeight="24px">
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </Typography>
            </Box>

            <Box sx={{ flex: "1" }}>
                <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                    Product
                </Typography>
                <Box style={flexContainer}>
                    {dataCategory.map((card, index) => {
                    return (
                        <Typography key={index} fontSize="14px" lineHeight="24px" textAlign="justify" color="#FFFFFF" sx={{ width: 'max-content' }}  style={flexItem}>
                            &bull; {card.name}
                        </Typography>
                    )
                    })}
                </Box>
            </Box>
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                flex: "2"
            }}>
                <Box>
                    <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                        Address
                    </Typography>
                    <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                    </Typography>
                </Box>
                <Box sx={{
                    marginTop: "16px"
                }}>
                    <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                        Contact Us
                    </Typography>
                    <Box sx={{
                        display: "flex",
                        columnGap: "16px",
                        padding: "none",
                        marginTop: "16px",
                        marginLeft: "8px"
                    }}>
                        <Box style={boxIconStyle}>
                            <CallIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <InstagramIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <YouTubeIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <TelegramIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <EmailIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Box>

    );
}

export default BannerBottom