import { Box } from '@mui/material'
import React from 'react'
import banner from '../../assets/banner-list-class.png'

const BannerListMenuClass = () => {
  return (
    <Box sx={{
        height: "274px", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", backgroundImage: `url(${banner})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
    }} />
  )
}

export default BannerListMenuClass