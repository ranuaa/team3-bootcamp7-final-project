import { Box, Container, Typography } from "@mui/material";
import React from "react";
import banner from '../../assets/banner-top.png'

const BannerTop = () => {
    return (
        <Box sx={{
            height: "274px", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", backgroundImage: `url(${banner})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
        }} >
            <Container>
                <Typography variant="h4" textAlign="center" color="#FFFFFF" >
                    Be the next great chef
                </Typography>
                <Typography variant="h6" textAlign="center" mt={2} color="#FFFFFF">
                    We are able to awaken your cooking skills to become a classy chef and
                    ready to dive into the professional world
                </Typography>
            </Container>
        </Box>
    );
};

export default BannerTop;
