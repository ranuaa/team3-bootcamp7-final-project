import { Box, Button, CardContent, CardMedia, Checkbox, IconButton, Typography } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { useNavigate } from 'react-router-dom';
import PaymentModal from './PaymentModal';
import { UserContext } from '../Context/UserContext'
import axios from 'axios';
import dayjs from "dayjs";


const CheckoutItem = () => {

    const baseUrl = process.env.REACT_APP_API
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const { user } = useContext(UserContext)
    const [checked, setChecked] = useState([]);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([])

    console.log(data)
    const handleFetchData = async (id) => {
        try {
            const response = await axios.get(`${baseUrl}User/GetUserCart?userId=${user.id}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                },
            })
            setData(response.data)
        } catch (error) {
            console.log(error)
        }
    }


    const handleDeleteCart = async (id) => {
        try {
            await await axios.delete(`${baseUrl}User/DeleteUserCart?cartItemId=${id}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                },
            })
            console.log(id)
            togleSelectAll()
            handleFetchData();
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleFetchData()
    }, []);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value.cartItemId);
        const newChecked = [...checked];
        if (currentIndex === -1) {
            newChecked.push(value.cartItemId);
            setTotal(total + value.bookingPrice);
        } else {
            newChecked.splice(currentIndex, 1);
            setTotal(total - value.bookingPrice);
        }
        setChecked(newChecked);
    };

    const togleSelectAll = () => {
        // check if all items are already checked
        const allChecked = data.every((data) => checked.includes(data.cartItemId));

        if (allChecked) {
            // if all items are already checked, uncheck all items
            setChecked([]);
            setTotal(0);
        } else {
            // if not all items are checked, check all items
            const newChecked = data.map((data) => data.cartItemId);
            const newTotal = data.reduce((acc, data) => {
                return acc + data.bookingPrice;
            }, 0);

            setChecked(newChecked);
            setTotal(newTotal);
        }
    };

    useEffect(() => {

    }, [checked]);


    return (
        <Box>
            <Box sx={{
                display: "flex",
                flexFlow: "row wrap",
                justifyContent: "flex-start",
                alignItems: "center",
                padding: "16px",
                columnGap: "24px",
                borderBottom: "2px solid #BDBDBD"
            }}>
                <Checkbox
                    checked={checked.length > 0 && checked.length == data.length}
                    onChange={togleSelectAll}
                    indeterminate={checked.length == data.length && data.length > 0}
                    size="large"
                    sx={{
                        color: "#5B4947", '&.Mui-checked': {
                            color: "#5B4947"
                        }
                    }}
                />
                <Typography
                    sx={{ fontSize: "20px", fontWeight: "400", lineHeight: "24px" }}
                    color="#333333"
                >
                    Pilih Semua
                </Typography>
            </Box>
            <Box sx={{ minHeight: "58vh" }}>
                {data.map((check, index) => {
                    return (
                        <Box key={index} sx={{ borderBottom: "2px solid #BDBDBD" }}>

                            <Box
                                sx={{
                                    display: "flex",
                                    lexDirection: "row",
                                    justifyContent: { sx: "center", md: "center", lg: "flex-start" },
                                    alignItems: "center",
                                    columnGap: "24px",
                                    padding: "20px 16px 20px 12px"
                                }}
                            >
                                <Checkbox
                                    type="checkbox"
                                    onChange={handleToggle(check)}
                                    checked={checked.indexOf(check.cartItemId) !== -1}
                                    size='large'
                                    sx={{
                                        color: "#5B4947", '&.Mui-checked': {
                                            color: "#5B4947"
                                        }
                                    }}
                                />

                                <Box sx={{ display: "flex", flexDirection: { xs: "column", md: "column", lg: "row" }, columnGap: "24px", rowGap: "16px", flex: { xs: "auto", md: "auto", lg: "1" }, justifyContent: "center", alignItems: "center" }}>
                                    <CardMedia
                                        component="img"
                                        sx={{ width: 200 }}
                                        image={check?.image}
                                    />
                                    <CardContent sx={{
                                        display: "flex",
                                        flexFlow: "row-wrap",
                                        flex: "1",
                                        flexDirection: "column",
                                        gap: "4px",
                                        textAlign: { xs: "center", md: "center", lg: "left" }
                                    }}>
                                        <Typography sx={{ fontSize: "16px", fontWeight: "400" }} color="#828282">
                                            {check.category}
                                        </Typography>
                                        <Typography
                                            sx={{ fontSize: "24px", fontWeight: "600" }}
                                            color="#333333"
                                            component="div"
                                        >
                                            {check.courseName}
                                        </Typography>
                                        <Typography
                                            sx={{ fontSize: "16px", fontWeight: "400" }}
                                            color="#4F4F4F"
                                            gutterBottom
                                            component="div"
                                        >
                                            {dayjs(check.bookingDate).format('dddd, DD MMMM YYYY')}
                                        </Typography>
                                        <Typography
                                            sx={{ fontSize: "20px", fontWeight: "600", lineHeight: "24px" }}
                                            color="#FABC1D"
                                            gutterBottom
                                            component="div"
                                        >
                                            IDR {check.bookingPrice?.toLocaleString("id-ID")}
                                        </Typography>
                                    </CardContent>
                                </Box>
                                <IconButton sx={{ padding: "0px" }}
                                    onClick={() => handleDeleteCart(check?.cartItemId)}>
                                    <DeleteForeverIcon sx={{ color: "#EB5757", fontSize: "2.5rem" }} />
                                </IconButton>

                            </Box>
                        </Box>
                    )
                })}
            </Box>
            <Box sx={{ display: "flex", padding: "32px 64px", marginTop: "64px", boxShadow: "0px 0px 10px #BDBDBD", alignItems: "center", columnGap: "28px" }}>
                <Typography
                    sx={{ fontSize: "20px", fontWeight: "500" }}
                    color="#4F4F4F"
                >
                    Total Price
                </Typography>
                <Typography
                    sx={{ fontSize: "24px", fontWeight: "600", lineHeight: "24px", flex: "auto" }}
                    color="#FABC1D"
                >
                    IDR {total.toLocaleString("id-ID")}
                </Typography>
                <Button
                    onClick={handleOpen}

                    sx={{ color: '#5B4947', borderRadius: "8px", display: 'block', backgroundColor: "#FABC1D", width: { xs: "150px", md: "200px", lg: "250px" }, textTransform: "none" }}
                    disabled={checked.length === 0}
                >
                    Pay Now
                </Button>
            </Box>
            <PaymentModal open={open} setOpen={setOpen} handleOpen={handleOpen} handleClose={handleClose} checked={checked} />
        </Box>
    )
}

export default CheckoutItem