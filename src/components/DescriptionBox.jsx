import { Box, Typography } from '@mui/material'
import React from 'react'

const DescriptionBox = ({ h1, text }) => {
  return (
    <Box sx={{ display: "flex", flexDirection: "column", padding: { xs: "46px 58px", md: "46px 78px", lg: "26px 72px" } }}>
      <Typography textAlign="left" sx={{ fontSize: "24px", fontWeight: "600" }} color="#000000" gutterBottom>
        {h1}
      </Typography>
      <Typography textAlign="justify" sx={{ mb: 1.5 }} color="#000000">
        {text}
      </Typography>
    </Box>
  )
}

export default DescriptionBox