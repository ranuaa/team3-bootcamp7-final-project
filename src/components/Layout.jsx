import React from "react";
import BannerBottom from "./Banners/BannerBottom";
import Navbar from "./Navbar";
import { Box } from "@mui/material";

const Layout = ({ children }) => {
    return (
        <Box sx={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
            <Navbar />
            <Box sx={{ flexGrow: "1" }}>
                {children}
            </Box>
            <BannerBottom />
        </Box>
    );
};

export default Layout;
