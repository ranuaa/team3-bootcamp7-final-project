import React from "react";
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import PaymentIcon from "@mui/icons-material/Payment";
import PeopleOutline from "@mui/icons-material/PeopleOutline";
import ReceiptIcon from "@mui/icons-material/Receipt";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import AdminRoutes from "../routes/AdminRoutes";


const drawerWidth = 240;

const Drawers = (props) => {

    const navigate = useNavigate()

    const { window } = props;
    const pages = [
        { name: "User", url: "./userpage", icon: <PeopleOutline /> },
        { name: "Payment", url: "./paymentmethod", icon: <PaymentIcon /> },
        { name: "Invoice", url: "./invoicepage", icon: <ReceiptIcon /> },
    ];
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <Toolbar sx={{ display: "flex", justifyContent: "center", color: '#fabc1d' }}>
                <Button onClick={() => navigate("/")} variant="text">
                    <Typography
                        variant="h5"
                        noWrap
                        sx={{
                            display: { xs: "flex" },
                            justifyContent: "center",
                            marginRight: "0px",
                            flexGrow: 1,
                            fontWeight: 700,
                            color: "#fabc1d",
                            textDecoration: "none",
                            textAlign: "center",
                        }}
                    >
                        Soup
                    </Typography>
                </Button>
            </Toolbar>

            <Divider />
            <List>
                {pages.map((page) => {
                    return (
                        <ListItem
                            key={page.name}
                            disablePadding
                            onClick={() => navigate(page.url)}
                        >
                            <ListItemButton>
                                <ListItemIcon>{page.icon}</ListItemIcon>
                                <ListItemText primary={page.name} />
                            </ListItemButton>
                        </ListItem>
                    );
                })}
            </List>
        </div>
    );

    const container =
        window !== undefined ? () => window().document.body : undefined;

    return (
        <Box sx={{ display: "flex" }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    ml: { sm: `${drawerWidth}px` },
                    backgroundColor: '#fabc1d'
                }}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: "none" } }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                        Admin Page
                    </Typography>
                </Toolbar>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                aria-label="mailbox folders"
            >
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: "block", sm: "none" },
                        "& .MuiDrawer-paper": {
                            boxSizing: "border-box",
                            width: drawerWidth,
                        },
                    }}
                >
                    {drawer}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: "none", sm: "block" },
                        "& .MuiDrawer-paper": {
                            boxSizing: "border-box",
                            width: drawerWidth,
                        },
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{
                    flexGrow: 1,
                    p: 3,
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                }}
            >
                <Toolbar />

                {/* Routing untuk page admin */}
                <AdminRoutes />
            </Box>
        </Box>

    )
};

export default Drawers;
