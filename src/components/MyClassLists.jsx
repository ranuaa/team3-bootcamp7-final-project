import React from "react";
import {
    Box,
    Card,
    CardActionArea,
    CardContent,
    CardMedia,
    Typography,
} from "@mui/material";
import dayjs from "dayjs";


const MyClassLists = ({ classes }) => {
    console.log(classes)
    return (
        <>
            {classes.map((cls) => {
                return (
                    <Card>
                        <CardActionArea>
                            <Box
                                sx={{
                                    display: "flex",
                                    flexFlow: "row wrap",
                                    justifyContent: "flex-start",
                                    margin: "40px 71px 24px"
                                }}
                            >
                                <CardMedia
                                    component="img"
                                    sx={{ width: 200 }}
                                    image={cls.image}
                                />
                                <CardContent>
                                    <Box
                                        sx={{
                                            display: "flex",
                                            flexDirection: "column",
                                            gap: "4px",
                                        }}
                                    >
                                        <Typography sx={{ fontSize: 14 }} color="text.secondary">
                                            {cls.category}
                                        </Typography>
                                        <Typography
                                            sx={{ fontSize: 16, fontWeight: "600" }}
                                            gutterBottom
                                            component="div"
                                        >
                                            {cls.name}
                                        </Typography>
                                    </Box>
                                    <Typography
                                        sx={{ fontSize: 20, fontWeight: "500" }}
                                        color="#FABC1D"
                                        gutterBottom
                                        component="div"
                                    >
                                        Schedule : {dayjs(cls.schedule).format('dddd, DD MMMM YYYY')}
                                    </Typography>
                                </CardContent>
                            </Box>
                        </CardActionArea>
                    </Card>
                )
            })}
        </>

    );
};

export default MyClassLists;
