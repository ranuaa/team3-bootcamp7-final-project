import { Box, FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react'
import { UserContext } from "../Context/UserContext";


const SelectLabels = ({ setDate }) => {
    const today = new Date();
    let futureDates = [];

    for (let i = 0; i < 7; i++) {
        const futureDate = new Date();
        futureDate.setDate(today.getDate() + i);
        futureDates.push(futureDate);
    }

    const baseUrl = process.env.REACT_APP_API
    const [schedule, setSchedule] = React.useState('');
    const { user } = useContext(UserContext);
    const [displayDate, setDisplayDate] = useState(futureDates)

    const handleChange = (event) => {
        const bookingDate = new Date()
        const selectedDate = new Date(event.target.value)
        // setDate(new Date(selectedDate.getDate()))
        setSchedule(event.target.value);
        setDate(new Date(selectedDate))
    };




    const options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };


    const handleFetchclass = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/GetUserClass?id=${user.id}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`
                }
            })
            const dates = response?.data
            const takenDate = []
            for (let i = 0; i < dates?.length; i++) {
                takenDate.push(new Date(dates[i].schedule).toLocaleDateString("en-GB", options))
            }
            const availdate = futureDates.filter(item => !takenDate?.includes(item.toLocaleDateString("en-GB", options)))
            handleFetchData(availdate)
        } catch (error) {
            console.log(error)
            const availdate = []
            handleFetchData(availdate)
        }
    }
    const handleFetchData = async (availdate) => {
        try {
            const response = await axios.get(`${baseUrl}User/GetUserCart?userId=${user.id}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                },
            })
            const dates = response?.data
            const takenDate = []
            for (let i = 0; i < dates?.length; i++) {
                takenDate.push(new Date(dates[i].bookingDate).toLocaleDateString("en-GB", options))
            }
            if (availdate?.length > 0) {
                const dats = availdate.filter(item => !takenDate?.includes(item.toLocaleDateString("en-GB", options)))
                setDisplayDate(dats)
            } else {
                const dats = futureDates.filter(item => !takenDate?.includes(item.toLocaleDateString("en-GB", options)))
                setDisplayDate(dats)
                setDisplayDate(futureDates)
            }
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        handleFetchclass()
    }, []);



    return (
        <Box>
            <FormControl sx={{ minWidth: 350, margin: "35px 0px" }} size='small'>
                <InputLabel>Select Schedule</InputLabel>
                <Select
                    value={schedule}
                    onChange={handleChange}
                    label="Select Schedule"
                    displayEmpty>
                    {displayDate?.map((date, index) => (
                        <MenuItem key={index} value={date.toLocaleDateString("en-GB", options)}>
                            {date.toLocaleDateString("en-GB", options)}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </Box>
    )
}

export default SelectLabels