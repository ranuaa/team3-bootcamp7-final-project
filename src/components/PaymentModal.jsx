import React, { useContext, useEffect, useState } from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useNavigate } from "react-router-dom";
import { UserContext } from '../Context/UserContext'
import axios from "axios";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    borderRadius: '10px',
    boxShadow: 24,
    p: 4,
};


const PaymentModal = ({ open, handleOpen, handleClose, setOpen, checked, handleAddtoCart }) => {

    const navigate = useNavigate()
    const baseUrl = process.env.REACT_APP_API
    const { user } = useContext(UserContext);
    const [selectedIndex, setSelectedIndex] = React.useState(1);
    const [data, setData] = useState([])

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    const handleFetchPayment = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/PaymentMethods`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                }
            })
            setData(response?.data)
        } catch (error) {
            console.log(error)
        }
    }

    const handleCreateInvoice = async () => {
        try {
            const response = await axios.post(`${baseUrl}User/CreateInvoice?userId=${user.id}`, {
                "cartItemIds": checked
            }, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                }
            })
            console.log(response.data)
            navigate('/success-purchase')
        } catch (error) {
            console.log(error)
        }
    }

    const handleDeleteCart = async (id) => {
        try {
            await axios.delete(`${baseUrl}User/DeleteUserCart?cartItemId=${checked[0]}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                },
            })
        } catch (error) {
            console.log(error)
        }
    }

    const handleCancel = () => {
        if (checked.length === 1) {
            handleDeleteCart()
            handleClose()
        } else {
            handleClose()
        }
    }
    useEffect(() => {
        handleFetchPayment()
    }, []);

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" textAlign='center'>
                        Select Payment Method
                    </Typography>
                    <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'white' }}>
                        <List component="nav" aria-label="main mailbox folders">
                            {data.map((dat, index) => {
                                return (
                                    <ListItemButton
                                        selected={selectedIndex === index}
                                        onClick={(event) => handleListItemClick(event, index)}
                                    >
                                        <ListItemIcon>
                                            <img src={dat?.logo} style={{ maxWidth: '40px', maxHeight: '40px' }} />
                                        </ListItemIcon>
                                        <ListItemText primary={dat.name} />
                                    </ListItemButton>
                                )
                            })}
                        </List>
                    </Box>
                    <Box sx={{ mt: '20px', display: 'flex', justifyContent: "space-evenly" }}>
                        <Button onClick={handleCancel} variant="outlined" sx={{ border: "2px solid #5B4947", color: '#5B4947', padding: '10px 40px' }}>Cancel</Button>
                        <Button onClick={() => handleCreateInvoice()} variant="contained" sx={{ backgroundColor: "#FABC1D", color: '#5B4947', padding: '10px 40px' }} >Pay Now</Button>
                    </Box>
                </Box>
            </Modal>
        </div>
    );
};

export default PaymentModal;
