import React, { useState } from 'react';

const data = [
  {
    img: 'pd1',
    category: 'Asian',
    name: 'Tom Yum Thailand',
    schedule: 'Schedule: Monday, 25 July 2022',
    price: 450000
  },
  {
    img: 'pd2',
    category: 'Asian',
    name: 'Ichiraku Ramen',
    schedule: 'Schedule: Monday, 25 July 2022',
    price: 300000
  },
  {
    img: 'pd3',
    category: 'Eastern',
    name: 'Italian Spaghetti Bolognese',
    schedule: 'Schedule: Monday, 25 July 2022',
    price: 45000
  },
];

function ForLoopExample() {
  const [selectAll, setSelectAll] = useState(false);
  const [checkboxes, setCheckboxes] = useState(() => {
    const initialState = {};
    for (let i = 0; i < data.length; i++) {
      initialState[`isChecked${i}`] = false;
    }
    return initialState;
  });

  const handleSelectAllChange = () => {
    const updatedSelectAll = !selectAll;
    setSelectAll(updatedSelectAll);
    setCheckboxes((prevCheckboxes) => {
      const updatedCheckboxes = { ...prevCheckboxes };
      for (let i = 0; i < data.length; i++) {
        updatedCheckboxes[`isChecked${i}`] = updatedSelectAll;
      }
      return updatedCheckboxes;
    });
  };

  const handleCheckboxChange = (index) => {
    setCheckboxes((prevCheckboxes) => {
      return {
        ...prevCheckboxes,
        [`isChecked${index}`]: !prevCheckboxes[`isChecked${index}`],
      };
    });
  };

  const checkedItems = data.filter((item, index) => checkboxes[`isChecked${index}`]);

  return (
    <div>
      <label>
        <input
          type="checkbox"
          checked={selectAll}
          onChange={handleSelectAllChange}
        />
        Select All
      </label>
      <br />
      {data.map((item, index) => (
        <label key={index}>
          <input
            type="checkbox"
            checked={checkboxes[`isChecked${index}`]}
            onChange={() => handleCheckboxChange(index)}
          />
          <img src={item.img} alt={item.name} />
          <div>{item.category}</div>
          <div>{item.name}</div>
          <div>{item.schedule}</div>
          <div>{item.price}</div>
        </label>
      ))}
      <br />
      <p>Selected items: {checkedItems.map((item) => item.price).join(', ')}</p>
    </div>
  );
}

export default ForLoopExample;
