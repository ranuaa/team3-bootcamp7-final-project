import Navbar from "./components/Navbar";
import LandingPage from "./pages/LandingPage";
import MainRoutes from "./routes/MainRoutes";
import { UserProvider } from './Context/UserContext'



function App() {
  return (
    <div >
      <UserProvider>
        <MainRoutes/>
      </UserProvider>
    </div>
  );
}

export default App;
