import { createContext, useState } from "react";

export const UserContext = createContext(null);

export const UserProvider = ({children}) => {

    const [user, setUser] = useState(null)

    const login = (userInfo) => {
        setUser(userInfo)
    }

    const logout = () => {
        setUser(null)
        localStorage.removeItem("useInfo")
    }
    return(
        <UserContext.Provider value={{user, login, logout}}>
            {children}
        </UserContext.Provider>
    )
}