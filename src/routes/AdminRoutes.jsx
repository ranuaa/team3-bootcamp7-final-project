import React, { useContext } from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import { UserContext } from '../Context/UserContext';
import InvoicePage from "../pages/AdminPages/InvoicePage";
import PaymentMethod from "../pages/AdminPages/PaymentMethod";
import UserPage from "../pages/AdminPages/UserPage";


const ProtectedRouteAdmin = ({ user, redirectPath = '/' }) => {
    if (!user && !user?.role) {
        return <Navigate to={redirectPath} replace />;
    }
    return <Outlet />;
};

const AdminRoutes = () => {
    const { user } = useContext(UserContext)
    return (
        <Routes>
            <Route element={<ProtectedRouteAdmin user={user} />} >
                <Route path="/invoicepage" element={<InvoicePage />} />
                <Route path="/paymentmethod" element={<PaymentMethod />} />
                <Route path="/userpage" element={<UserPage />} />
            </Route>
        </Routes>
    );
};

export default AdminRoutes;
