import React, { useContext } from "react";
import { Navigate, Route, Routes, Outlet } from "react-router-dom";
import DetailInvoice from "../pages/DetailInvoice";
import Invoice from "../pages/Invoice";
import LandingPage from "../pages/LandingPage";
import LoginRegister from "../pages/LoginRegister";
import MyClass from "../pages/MyClass";
import ResetPassword from "../pages/ResetPassword";
import SuccessPage from "../pages/SuccessPage";
import DetailClass from "../pages/DetailClass";
import ListMenuClass from "../pages/ListMenuClass";
import Checkout from "../pages/Checkout";
import { UserContext } from '../Context/UserContext'
import Dashboard from "../pages/AdminPages/Dashboard";

const ProtectedRoute = ({ user, redirectPath = '/login' }) => {
  if (!user) {
    return <Navigate to={redirectPath} replace />;
  }
  return <Outlet />;
};

const ProtectedRouteAdmin = ({ user, redirectPath = '/' }) => {
  if (!user && !user?.role) {
    return <Navigate to={redirectPath} replace />;
  }
  return <Outlet />;
};

const MainRoutes = () => {
  const { user } = useContext(UserContext)
  return (
    <div>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/login" element={<LoginRegister page="login" />} />
        <Route path="/register" element={<LoginRegister page="register" />} />
        <Route path="/reset-password-email" element={<ResetPassword page="email" />} />
        <Route path="/reset-password-newpass" element={<ResetPassword page="newpass" />} />
        <Route path="/email-confirm" element={<SuccessPage page="email" />} />
        <Route element={<ProtectedRoute user={user} />}>
          <Route path="/myclass" element={<MyClass />} />
          <Route path="/success-purchase" element={<SuccessPage page="purchase" />} />
          <Route path="/invoice" element={<Invoice />} page="invoice" />
          <Route path="/invoice-detail/:id" element={<DetailInvoice />} />
          <Route path="/list-menu-class/:category" element={<ListMenuClass />} />
          <Route path="/detail-class/:id" element={<DetailClass />} />
          <Route path="/checkout" element={<Checkout />} />
        </Route>
        <Route element={<ProtectedRouteAdmin user={user} />} >
          <Route path="/dashboard/*" element={<Dashboard />} />
        </Route>
      </Routes>
    </div>
  );
};

export default MainRoutes;
