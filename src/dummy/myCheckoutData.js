import pd1 from "../assets/tom-yum-thailand.png"
import pd2 from "../assets/ichiraku-ramen.png"
import pd3 from "../assets/italian-spaghetti-bolognese.png"

export const checkoutData = [
    {
        img: pd1,
        category: "Asian",
        name: "Tom Yum Thailand",
        schedule:"Schedule : Monday, 25 July 2022",
        price: 450000
    },
    {
        img: pd2,
        category: "Asian",
        name: "Ichiraku Ramen",
        schedule:"Schedule : Monday, 25 July 2022",
        price: 300000
    },
    {
        img: pd3,
        category: "Eastern",
        name: "Italian Spaghetti Bolognese",
        schedule:"Schedule : Monday, 25 July 2022",
        price: 450000
    }
]