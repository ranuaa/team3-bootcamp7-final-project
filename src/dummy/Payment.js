import Dana from '../assets/Dana.png'
import BCA from '../assets/BCA.png'
import Ovo from '../assets/Ovo.png'
import Gopay from '../assets/Gopay.png'
import Mandiri from '../assets/Mandiri.png'
import BNI from '../assets/BNI.png'



export const PaymentList = [
    {
        img : Gopay,
        name: "Gopay"
    },
    {
        img : Ovo,
        name: "OVO"
    },
    {
        img : Dana,
        name: "DANA"
    },
    {
        img : Mandiri,
        name: "MANDIRI"
    },
    {
        img : BCA,
        name: "BCA"
    },
    {
        img : BNI,
        name: "BNI"
    },
]