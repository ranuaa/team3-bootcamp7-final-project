import pd1 from "../assets/sushi-salmon-with-mentai.png"
import pd2 from "../assets/ichiraku-ramen.png"
import pd3 from "../assets/rendang-sapi.png"
import pd4 from "../assets/pempek.png"
import pd5 from "../assets/soto-banjar-limau-kuit.png"
import pd6 from "../assets/tom-yum-thailand.png"
import pd8 from "../assets/Jajangmeyon.png"
import pd9 from "../assets/sate-padang.png"

export const detailDatas = [
    {
        img: pd1,
        category: "Asian",
        name: "Sushi Salmon with Mentai",
        price: "IDR 200.000"
    },
    {
        img: pd2,
        category: "Asian",
        name: "Ichiraku Ramen",
        price: "IDR 300.000"
    },
    {
        img: pd3,
        category: "Asian",
        name: "Rendang Sapi",
        price: "IDR 200.000"
    },
    {
        img: pd4,
        category: "Asian",
        name: "[Complit Package] Pempek Palembang",
        price: "IDR 600.000"
    },
    {
        img: pd5,
        category: "Asian",
        name: "Soto Banjar Limau Kuit",
        price: "IDR 150.000"
    },
    {
        img: pd6,
        category: "Asian",
        name: "Tom Yum Thailand",
        price: "IDR 450.000"
    },
    {
        img: pd8,
        category: "Asian",
        name: "Jajangmeyon",
        price: "IDR 250.000"
    },
    {
        img: pd9,
        category: "Asian",
        name: "Sate Padang",
        price: "IDR 300.000"
    }
]
