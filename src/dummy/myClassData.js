import pd1 from "../assets/strawberry-float.png"
import pd2 from "../assets/choco-cookie.png"
import pd3 from "../assets/green-tea-cheese-cake.png"
import pd4 from "../assets/soto-banjar-limau-kuit.png"
import pd5 from "../assets/italian-spaghetti-bolognese.png"
import pd6 from "../assets/tom-yum-thailand.png"

export const cardClassDatas = [
    {
        img: pd6,
        category: "Asian",
        name: "Tom Yum Thailand",
        price: "IDR 450.000"
    },
    {
        img: pd1,
        category: "Cold Drink",
        name: "Strawberry Float",
        price: "IDR 150.000"
    },
    {
        img: pd2,
        category: "Cookies",
        name: "Chocholate Cookies",
        price: "IDR 200.000"
    },
    {
        img: pd3,
        category: "Dessert",
        name: "Green Tea Cheesecake",
        price: "IDR 400.000"
    },
    {
        img: pd4,
        category: "Asian",
        name: "Soto Banjar Limau Kuit",
        price: "IDR 150.000"
    },
    {
        img: pd5,
        category: "Western",
        name: "Italian Spaghetti Bolognese",
        price: "IDR 450.000"
    },
]
