import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Button, Container, Grid, Typography } from "@mui/material";
import Stack from "@mui/material/Stack";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import React, { useContext, useEffect, useState } from "react";
import Navbar from "../components/Navbar";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import IconButton from "@mui/material/IconButton";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../Context/UserContext";
import isEmail from "validator/lib/isEmail";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';



const LoginRegister = ({ page }) => {
  const baseUrls = process.env.REACT_APP_API;

  const navigate = useNavigate();
  const [isDisabled, setDisabled] = useState(false)
  const [open, setOpen] = React.useState(false);
  const [alert, setAlert] = useState({ color: "", text: "" });
  const [showPassword, setShowPassword] = React.useState(false);
  const { user, login } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");


  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  console.log(user)
  useEffect(() => {
    if (localStorage.getItem("useInfo")) {
      navigate("/");
    }
  }, [user]);

  const handleLogin = async (e) => {
    e.preventDefault();
    setDisabled(true)
    if (
      email === "" || password === "") {
      toast.warn("Please fill all the form field")
      setDisabled(false)
    } else {
      toast.info("Please wait you are being redirected")
      setDisabled(true)
      try {
        const response = await axios.post(`${baseUrls}User/Login`, {
          password: password,
          email: email
        });
        localStorage.setItem("useInfo", JSON.stringify(response.data))
        await login(response?.data)
        if (user) {
          setEmail("");
          setPassword("");
          setName("");
          setConfirmPassword("");
          setDisabled(false)
          navigate("/");
        }
      } catch (error) {
        toast.error(error?.response?.data)
        setDisabled(false)
      }
    }
  };



  const handleSignUp = async (e) => {
    e.preventDefault();
    if (
      email === "" ||
      name === "" ||
      password === "" ||
      confirmPassword === ""
    ) {
      toast.warn("Please fill all the form field");
    } else if (!isEmail(email)) {
      toast.warn("Please check your email");
    } else if (password !== confirmPassword) {
      toast.warn("Please provide match password with confirm password");
    } else {
      toast.info("Please wait you are being redirected")
      setDisabled(true)
      try {
        const response = await axios.post(`${baseUrls}User/Register`, {
          username: name,
          password: password,
          email: email,
          role: false,
          status: false,
        });
        if (response?.length !== 0) {
          setDisabled(false)
          navigate("/");
          setEmail("");
          setPassword("");
          setName("");
          setConfirmPassword("");
        }
      } catch (error) {
        toast.error(error?.response?.data)
      }
    }
  };



  return (
    <>
      <Navbar />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      {page === "login" ? (
        <Container
          maxWidth="xl"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
          }}>
          <Box sx={{ width: { xs: "100%", sm: "70%" } }}>
            <Box mb={5}>
              <Typography variant="h4" sx={{ color: "#5B4947", fontWeight: "500" }}>
                Welcome Back! Cheff
              </Typography>
              <Typography variant="h6" sx={{ color: "#5B4947", fontWeight: "500" }}>
                Please login first
              </Typography>
            </Box>
            <form>
              <FormControl fullWidth sx={{ mb: 1 }} variant="outlined">
                <InputLabel htmlFor="email">Email</InputLabel>
                <OutlinedInput
                  id="email"
                  type="email"
                  label="email"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormControl>
              <FormControl fullWidth sx={{ mb: 1 }} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? "text" : "password"}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end">
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>
              <Typography textAlign="start">
                Forgot Password?{" "}
                <span
                  style={{ color: "#2F80ED", cursor: "pointer" }}
                  onClick={() => navigate("/reset-password-email")}>
                  Click Here
                </span>
              </Typography>
              <Box mt={2} sx={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  sx={{ backgroundColor: "#FABC1D", color: "#5B4947", width: "100px" }}
                  onClick={handleLogin}
                  isDisabled={isDisabled}
                >
                  Login
                </Button>
              </Box>
            </form>
            <Typography textAlign="center" mt={2}>
              Don't have account?{" "}
              <span
                style={{ color: "#2F80ED", cursor: "pointer" }}
                onClick={() => navigate("/register")}>
                Sign Up here
              </span>
            </Typography>
          </Box>
        </Container>
      ) : (
        <Container
          maxWidth="xl"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh"
          }}
          >
          <Box sx={{ width: { xs: "100%", sm: "70%" } }} mt={{xs:0, md:0, lg:5}}> 
            <Box mb={5}>
              <Typography variant="h4" sx={{ color: "#5B4947", fontWeight: "500" }}>
                Are you ready become a professional chef?
              </Typography>
              <Typography variant="h6" sx={{ color: "#5B4947", fontWeight: "500" }}>
                Please register first
              </Typography>
            </Box>
            <form>
              <FormControl fullWidth sx={{ mb: 1 }}variant="outlined">
                <InputLabel htmlFor="email">Name</InputLabel>
                <OutlinedInput
                  id="name"
                  type="text"
                  label="name"
                  onChange={(e) => setName(e.target.value)}
                />
              </FormControl>
              <FormControl fullWidth sx={{ mb: 1 }} variant="outlined">
                <InputLabel htmlFor="email">Email</InputLabel>
                <OutlinedInput
                  id="email"
                  type="email"
                  label="email"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormControl>
              <FormControl fullWidth sx={{ mb: 1 }} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? "text" : "password"}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end">
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>
              <FormControl fullWidth sx={{ mb: 1 }} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Confirm Password</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? "text" : "password"}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end">
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Confirm Password"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </FormControl>
              <Box mt={2} sx={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  sx={{ backgroundColor: "#FABC1D", color: "#5B4947", width: "100px" }}
                  onClick={handleSignUp}
                  disabled={isDisabled}
                >
                  Register
                </Button>
              </Box>
            </form>
            <Typography textAlign="center" mt={2}>
              Have account?{" "}
              <span
                style={{ color: "#2F80ED", cursor: "pointer" }}
                onClick={() => navigate("/login")}>
                Login here
              </span>
            </Typography>
          </Box>
        </Container>
      )}
    </>
  );
};

export default LoginRegister;
