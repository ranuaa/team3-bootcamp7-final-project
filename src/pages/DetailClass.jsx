import React, { useContext, useEffect, useState } from 'react'
import Layout from '../components/Layout'
import { Box, Typography } from '@mui/material'
import MenuCard from '../components/Cards/MenuCard'
import DescriptionBox from '../components/DescriptionBox'
import pd6 from "../assets/tom-yum-thailand.png"
import DetailCards from '../components/Cards/DetailCards'
import { detailDatas } from '../dummy/myDetailData'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import axios from 'axios'
import { UserContext } from '../Context/UserContext'
import PaymentModal from '../components/PaymentModal'
import dayjs from 'dayjs'
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const DetailClass = () => {
  const navigate = useNavigate()
  const { id } = useParams()
  const h1 = "Description"
  const baseUrl = process.env.REACT_APP_API
  const { user } = useContext(UserContext)
  const location = useLocation();
  const [dataCourseDesc, setDataCourseDesc] = useState([]);
  const [dataCourse, setDataCourse] = useState(null);
  const [date, setDate] = useState("")
  const [render, setRender] = useState(false)
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [checked, setChecked] = useState([])


  const handleFetchData = async () => {
    const utcTime = dayjs(date)
    try {
      const response = await axios.get(`${baseUrl}User/GetUserCart?userId=${user.id}`, {
        headers: {
          Authorization: `Bearer ${user.token}`,
          ContentType: "application/json",
        },
      })
      const cartItems = response?.data
      const data = cartItems.filter(function (cart) {
        console.log(dayjs(cart.bookingDate).format("DD-MM-YYYY"))
        console.log(dayjs(utcTime.add(3, 'hours')).format("DD-MM-YYYY"))
        return cart.courseId == id && dayjs(utcTime.add(3, 'hours')).format("DD-MM-YYYY") === dayjs(cart.bookingDate).format("DD-MM-YYYY")
      })
      setChecked(data)
    } catch (error) {
      console.log(error)
    }
  }


  const handleAddtoCart = async (props) => {
    const utcTime = dayjs(date)
    const currentTime = utcTime.add(3, 'hours').format()
    console.log(dayjs(utcTime).format())
    if (props !== "buyNow") {
      try {
        const response = await axios.post(`${baseUrl}User/InsertCart`,
          {
            userId: user.id,
            courseId: dataCourseDesc.id,
            name: dataCourseDesc.name,
            bookingDate: currentTime,
            bookingPrice: dataCourseDesc.price
          },
          {
            headers: {
              Authorization: `Bearer ${user.token}`,
              ContentType: "application/json",
            }
          })
        getCoursedetail()
        navigate('/checkout')
      } catch (error) {
        // toast.warn("please select schedule");
        console.log(error)
      }
    } else {
      try {
        await axios.post(`${baseUrl}User/InsertCart`,
          {
            userId: user.id,
            courseId: dataCourseDesc.id,
            name: dataCourseDesc.name,
            bookingDate: currentTime,
            bookingPrice: dataCourseDesc.price
          },
          {
            headers: {
              Authorization: `Bearer ${user.token}`,
              ContentType: "application/json",
            }
          })
        getCoursedetail()
        handleFetchData()
      } catch (error) {
        console.log(error)
        toast.warn("please select schedule");
      }
    }
  }

  const buyNow = async () => {
    try {
      const response = await axios.post(`${baseUrl}User/InsertCart`,
        {
          userId: user.id,
          courseId: dataCourseDesc.id,
          name: dataCourseDesc.name,
          bookingDate: date,
          bookingPrice: dataCourseDesc.price
        },
        {
          headers: {
            Authorization: `Bearer ${user.token}`,
            ContentType: "application/json",
          }
        })
      return
    } catch (error) {
      console.log(error)
      toast.warn("please select schedule");
    }
  }



  const getCoursedetail = async () => {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API}User/GetCourseDetail?id=${id}`)
      setDataCourseDesc(response.data[0])
      await getCourseByCategory(response.data[0])
    } catch (error) {
      console.log(error)
    }
  }

  const getCourseByCategory = async (dat) => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API}User/GetCourseByCategory?id=${dat.id_categories}`
      );
      let data = response?.data;
      data = data.filter(course => course.id !== dat.id);
      setDataCourse(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    getCoursedetail()
  }, [id]);

  console.log(dataCourse);


  return (
    <Layout>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <Box sx={{ marginTop: "46px", padding: { xs: "0px", md: "0px 53px", lg: "0px 98px" } }}>
        <DetailCards img={dataCourseDesc.image} category={dataCourseDesc.categories} name={dataCourseDesc.name} price={dataCourseDesc.price} handleAddtoCart={handleAddtoCart} setDate={setDate} render={render} handleOpen={handleOpen} handleClose={handleClose} />
      </Box>
      <Box sx={{ borderBottom: "2px solid #E0E0E0" }}>
        <DescriptionBox h1={h1} text={dataCourseDesc.description} />
      </Box>
      <Typography variant="h4" textAlign="center" mt={5} color="#5B4947" style={{ margin: "64px 0px" }}>
        Another menu in this class
      </Typography>
      <Box sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "64px", alignItems: "center", rowGap: "32px", flexWrap: "wrap", padding: { xs: "0px", md: "0px 23px", lg: "0px 53px" } }}  >
        {dataCourse?.map((card, index) => {
          return (
            <MenuCard key={index} id={card.id} id_categories={card.id_categories} img={card.image} category={card.categories} name={card.name} price={card.price} />
          )
        })}
      </Box>
      <PaymentModal open={open} setOpen={setOpen} handleClose={handleClose} checked={[checked[0]?.cartItemId]} handleAddtoCart={handleAddtoCart} />
    </Layout>
  );
};

export default DetailClass;