import React, { useContext, useEffect, useState } from "react";
import Layout from "../components/Layout";
import MyClassLists from "../components/MyClassLists";
import { Box, Typography } from "@mui/material";
import { UserContext } from "../Context/UserContext";
import axios from "axios";


const MyClass = () => {
    const baseUrl = process.env.REACT_APP_API
    const { user } = useContext(UserContext);
    const [classes, setClasses] = useState([])

    const handleFetchData = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/GetUserClass?id=${user.id}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`
                }
            })
            setClasses(response?.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleFetchData()
    }, []);
    return (
        <Layout>
            {classes.length > 0 ? <MyClassLists classes={classes} /> : <Typography>No Class Yet</Typography>}

        </Layout>
    );
};

export default MyClass;
