import { Button, Container, Typography } from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import LandingPageCards from "../components/Cards/LandingPageCards";
import MyClassCards from "../components/Cards/MyClassCards";
import Layout from "../components/Layout";
import { cardClassDatas } from "../dummy/myClassData.js"
import BannerTop from "../components/Banners/BannerTop";
import BannerMid from "../components/Banners/BannerMid";
import { cardTypeFoodDatas } from "../dummy/myFoodtype";
import MyFoodTypeCards from "../components/Cards/MyFoodTypeCards";
import BannerBottom from "../components/Banners/BannerBottom";
import { useNavigate } from "react-router-dom";
import { UserContext } from '../Context/UserContext'
import axios from "axios";


const cardDatas = [
  {
    h1: "200+",
    text: "Various cuisines available in professional class"
  },
  {
    h1: "50+",
    text: "A chef who is reliable and has his own characteristics in cooking"
  },
  {
    h1: "30+",
    text: "Cooperate with trusted and upscale restaurants"
  }
]

const LandingPage = () => {

  const navigate = useNavigate()
  const { login, user } = useContext(UserContext)
  const [dataCategory, setDataCategory] = useState([]);
  const [dataCourse, setDataCourse] = useState([]);

  const getCategory = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API}User/GetAllCategories`
      );
      const data = response.data;
      setDataCategory(data);
    } catch (error) {
      console.log(error);
    }
  };

  const getCourse = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API}User/GetAllCourse`
      );
      const data = response.data;
      const sumCourse = cardDatas[0].h1 = `${data.length}+`
      const dataSlice  = data.slice(0, 6);
      setDataCourse(dataSlice);
    } catch (error) {
      console.log(error);
    }
  };



  useEffect(() => {
    const userCredential = localStorage.getItem("useInfo")
    if (userCredential) {
      login(JSON.parse(userCredential))
    }
    getCategory();
    getCourse();
    console.log(user)
  }, []);

  return (
    <Layout>
      <BannerTop />
      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "32px", marginBottom: "32px", alignItems: "center", gap: "10px" }}  >
        {cardDatas.map((card, index) => {
          return (
            <LandingPageCards key={index} h1={card.h1} text={card.text} />
          )
        })}
      </Container>

      <Typography variant="h4" textAlign="center" mt={5} ml={5} mr={5} color="#5B4947">
        More professional class
      </Typography>

      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "50px", alignItems: "center", gap: "10px", flexWrap: "wrap" }}  >
        {dataCourse.map((card, index) => {
          return (
            <MyClassCards key={index} id={card.id} id_categories={card.id_categories} img={card.image} category={card.categories} name={card.name} price={card.price}
            />
          )
        })}
      </Container>

      <BannerMid />

      <Typography variant="h4" textAlign="center" mt={5} ml={5} mr={5} color="#5B4947" sx={{ marginTop: "64px" }}>
        More food type as you can choose
      </Typography>

      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "64px", alignItems: "center", gap: "10px", flexWrap: "wrap" }}  >
        {dataCategory.map((card, index) => {
          return (
            <MyFoodTypeCards key={index} img={card.imageIcon} category={card.name} id={card.id} />
          )
        })}
      </Container>

    </Layout>
  );
};

export default LandingPage;
