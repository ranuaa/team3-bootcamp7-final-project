import React from 'react'
import Navbar from '../components/Navbar'
import CheckoutItem from '../components/CheckoutItem'


const Checkout = () => {

  return (
    <>
      <Navbar />
      <CheckoutItem />
    </>
  )
}

export default Checkout