import { Box, Container, Typography } from "@mui/material";
import React, { useState, useEffect, useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import BreadCrumb from "../components/BreadCrumb";
import Layout from "../components/Layout";
import Tables from "../components/Tables";
import dayjs from "dayjs";
import axios from 'axios'
import { UserContext } from '../Context/UserContext'

function createData(No, CourseName, Type, Schedule, Price) {
    return { No, CourseName, Type, Schedule, Price };
}

const rows = [
    createData('1', 'Tom Yum Thailand', 'Asian', 'Wednesday, 27 July 2022', 'IDR 450.000')
];

const DetailInvoice = () => {
    const baseUrl = process.env.REACT_APP_API
    const location = useLocation()
    const navigate = useNavigate()
    const { user } = useContext(UserContext)
    const [page, setPage] = useState(location.state.page)
    const [invoiceData, setInvoiceData] = useState(location.state.data)
    const [row, setRow] = useState([])
    const breadcrumbsDetail = [
        <Typography onClick={() => navigate('/')} sx={{ cursor: 'pointer' }}>
            Home
        </Typography>,
        <Typography onClick={() => navigate('/invoice')} sx={{ cursor: 'pointer' }} >
            Invoice
        </Typography>,
        <Typography sx={{ textDecoration: 'underline' }}>
            Details Invoice
        </Typography>
    ];

    const handleFetchDetail = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/GetInvoiceItems?invoiceId=${invoiceData?.invoiceId}`, {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    ContentType: "application/json",
                }
            })
            setRow(response?.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleFetchDetail()
    }, []);

    return (
        <Layout>
            <Container sx={{ marginTop: '15px' }}>
                <BreadCrumb pages={breadcrumbsDetail} />
                <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F', marginTop: '20px' }}>
                    Details Invoice
                </Typography>
                <Box>
                    <Box>
                        <Typography sx={{ color: '#4F4F4F', marginTop: '20px' }}>
                            No. Invoice &nbsp;:&nbsp; {invoiceData?.invoiceId}
                        </Typography>
                    </Box>

                    <Box sx={{ display: 'flex', width: '100%', justifyContent: 'space-between', marginTop: '10px', marginBottom: '20px' }}>
                        <Typography sx={{ color: '#4F4F4F' }}>
                            Date &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;: &nbsp;{dayjs(invoiceData?.orderDate).format('dddd, DD MMMM YYYY')}
                        </Typography>
                        <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F' }}>
                            Total Price : IDR {invoiceData?.totalPrice?.toLocaleString("id-ID")}
                        </Typography>
                    </Box>

                </Box>
                <Tables page='invoice' rows={row} heads={['No', 'CourseName', 'Type', 'Schedule', 'Price']} />
            </Container>
        </Layout>
    );
};

export default DetailInvoice;
