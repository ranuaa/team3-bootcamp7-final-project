import React, { useContext, useEffect, useState } from "react";
import Layout from "../components/Layout";
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import BreadCrumb from "../components/BreadCrumb";
import { useNavigate } from "react-router-dom";
import { Container } from "@mui/material";
import Tables from "../components/Tables";
import { UserContext } from '../Context/UserContext'
import axios from "axios";






const Invoice = ({ page }) => {
    const navigate = useNavigate()
    const baseUrl = process.env.REACT_APP_API
    const { user } = useContext(UserContext);
    const [row, setRow] = useState([])
    const [newRow, setNewRow] = useState([])

    const breadcrumbsInvoice = [
        <Typography onClick={() => navigate('/')} sx={{ cursor: 'pointer' }}>
            Home
        </Typography>,
        <Typography sx={{ textDecoration: 'underline' }}>
            Invoice
        </Typography>
    ]

    const handleFetchData = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/GetUserInvoices?userId=${user.id}`)
            setRow(response?.data.reverse())
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleFetchData()
        
    }, []);

    console.log(row)
    return (
        <Layout>
            <Container sx={{ marginTop: '15px' }}>
                <BreadCrumb pages={breadcrumbsInvoice} />
                <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F', marginTop: '20px' }}>
                    Menu Invoice
                </Typography>
                <Tables page='invoice' rows={row} heads={['No', 'No.Invoice', 'Date', 'Total Course', 'Total Price', 'Action']} />
            </Container>
        </Layout>
    );
};

export default Invoice;
