import { Button, Container, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect } from "react";
import { useLocation, useNavigate, useParams, useSearchParams } from "react-router-dom";
import success from '../assets/success.png'
import NavbarLogoOnly from "../components/NavbarLogoOnly";
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import axios from "axios";

const SuccessPage = ({ page }) => {
    const navigate = useNavigate()

    let search = window.location.search

    const handleActivate = async () => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API}User/ActivateUser${search}`)
            console.log(response)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleActivate()
    }, []);


    return (
        <>
            <NavbarLogoOnly />
            {page === 'email' ?
                <Container sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                    <img src={success} alt="success" />
                    <Typography textAlign='center' variant="h4" m={1}>
                        Email Confirmation Success
                    </Typography>
                    <Typography m={1}>
                        Congratulations! your email has already used.
                    </Typography>
                    <Button
                        onClick={() => navigate('/login')}
                        sx={{ margin: "0 20px", color: '#5B4947', display: 'block', backgroundColor: "#FABC1D" }}
                    >
                        Login
                    </Button>
                </Container>
                :
                <Container sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                    <img src={success} alt="success" />
                    <Typography textAlign='center' variant="h4" m={1}>
                        Purchase Successfully
                    </Typography>
                    <Typography textAlign='center' m={1}>
                        Horay!! Let’s cook and become a professional cheff
                    </Typography>
                    <Stack direction={{ md: 'row', xs: 'column' }} spacing={2}>

                        <Button sx={{ color: '#5B4947' }} variant="outlined" startIcon={<HomeIcon />} onClick={() => navigate('/')}>
                            Back to Home
                        </Button>
                        <Button onClick={() => navigate('/invoice')} variant="contained" startIcon={<ArrowForwardIcon />} sx={{ backgroundColor: '#FABC1D', color: '#5B4947' }}>
                            Open Invoice
                        </Button>
                    </Stack>
                </Container>
            }
        </>
    );
};

export default SuccessPage;
