import React, { useEffect, useState } from 'react'
import Layout from '../components/Layout'
import BannerListMenuClass from '../components/Banners/BannerListMenuClass'
import { Box, Typography } from '@mui/material'
import DescriptionBox from '../components/DescriptionBox'
import MyClassCards from '../components/Cards/MyClassCards'
import MenuCard from '../components/Cards/MenuCard'
import { useLocation } from 'react-router-dom'
import axios from 'axios'

const ListMenuClass = () => {
  const location = useLocation();
  const id = location.state;
  const [dataCourse, setDataCourse] = useState([]);
  const [dataCategory, setDataCategory] = useState([]);

  const getCategoryById = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API}User/GetCategoriesById?id=${id}`
      );
      const data = response.data;
      setDataCategory(data);
    } catch (error) {
      console.log(error);
    }
  };

  const getCourseByCategory = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API}User/GetCourseByCategory?id=${id}`
      );
      const data = response.data;
      setDataCourse(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCourseByCategory();
    getCategoryById();
  }, []);

  return (
    <Layout>
      <BannerListMenuClass />
      <Box sx={{ borderBottom: "2px solid #E0E0E0" }}>
        <DescriptionBox h1={dataCategory.name} text={dataCategory.description} />
      </Box>
      <Typography variant="h4" textAlign="center" mt={5} ml={5} mr={5} color="#5B4947" style={{ margin: "64px 0px" }}>
        Another menu in this class
      </Typography>
      <Box sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "64px", alignItems: "center", rowGap: "32px", flexWrap: "wrap", padding: { xs: "0px", md: "0px 23px", lg: "0px 53px" } }}  >
        {dataCourse.map((card, index) => {
          return (
            <MenuCard key={index} id={card.id} id_categories={card.id_categories} img={card.image} category={card.categories} name={card.name} price={card.price} />
          )
        })}
      </Box>
    </Layout>

  )
}

export default ListMenuClass