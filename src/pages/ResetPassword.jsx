import FormControl from '@mui/material/FormControl';
import { Box, Button, InputLabel, OutlinedInput, Typography } from "@mui/material";
import React, { useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const ResetPassword = ({ page }) => {


    const [email, setEmail] = useState("")
    const [isLoading, setIsloading] = useState(false)
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")


    let search = window.location.search


    const navigate = useNavigate()

    console.log(password)
    console.log(confirmPassword)

    const handleSendEmail = async () => {
        setIsloading(true)
        toast.info("Please wait a moment")
        try {
            const response = await axios.post(`${process.env.REACT_APP_API}User/EmailResetPassword`, {
                email: email
            })
            toast(response?.data, {
                onClose: () => navigate('/')
            });
            setEmail("")
        } catch (error) {
            toast.error(error?.response?.data)
            setIsloading(false)
        }
    }

    const handleClear = () => {
        setEmail("")
        setPassword("")
        setConfirmPassword("")
        navigate('/login')
    }

    const changePassword = async () => {
        setIsloading(true)
        if (password === "" || confirmPassword === "") {
            toast.error("Please Input all the necessary fields")
        } else if (password !== confirmPassword) {
            toast.error("password And confirm password do not match")
        } else {
            toast.info("Please check your email, Please wait a moment you are being redirected")
            try {
                const response = await axios.post(`${process.env.REACT_APP_API}User/ResetPassword`, {
                    email: search?.split("&Token")[0].split("=")[1],
                    password: password,
                    token: search?.split("&Token")[1].split("=")[1].replace('%', "/")
                })
                toast(response?.data, {
                    onClose: () => handleClear()
                });

            } catch (error) {
                toast.error(error?.response?.data)
                setIsloading(false)
            }
        }
    }

    return (
        <Box>
            <Navbar />
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            {page === "email" ?
                <Box sx={{ width: '100%', height: "100vh", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                    <Box sx={{ display: "flex", flexDirection: "column", width: "80%" }} >
                        <Typography variant="h4" mb={1}>Reset Password</Typography>
                        <Typography variant="h6" mb={1}>Send OTP code to your email address</Typography>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="email">Email</InputLabel>
                            <OutlinedInput
                                id="email"
                                type="email"
                                label="email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </FormControl>
                    </Box>
                    <Box sx={{ display: "flex", justifyContent: "end", width: "80%", gap: { xs: "1rem", sm: "1rem", md: "2rem" }, flexDirection: { xs: "column", sm: "column", md: "row" } }} >
                        <Button variant="outlined" onClick={() => navigate('/')} disabled={isLoading} >
                            <Typography sx={{ color: "#5B4947" }}>
                                Cancel
                            </Typography>
                        </Button>
                        <Button sx={{ backgroundColor: "#EA9E1F" }} disabled={isLoading}>
                            <Typography sx={{ color: "#5B4947" }} onClick={() => handleSendEmail()}>
                                Confirm
                            </Typography>
                        </Button>
                    </Box>
                </Box>
                :
                <Box sx={{ width: '100%', height: "100vh", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                    <Box sx={{ display: "flex", flexDirection: "column", width: "80%" }} >
                        <Typography variant="h4" mb={1} >Create Password</Typography>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="password">Password</InputLabel>
                            <OutlinedInput
                                id="password"
                                type="password"
                                label="password"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </FormControl>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="confirmPassword">Confirm Password</InputLabel>
                            <OutlinedInput
                                id="confirmPassword"
                                type="password"
                                label="confirmPassword"
                                onChange={(e) => setConfirmPassword(e.target.value)}
                            />
                        </FormControl>
                    </Box>
                    <Box sx={{ display: "flex", justifyContent: "end", width: "80%", gap: { xs: "1rem", sm: "1rem", md: "2rem" }, flexDirection: { xs: "column", sm: "column", md: "row" } }} >
                        <Button sx={{ backgroundColor: "#EA9E1F" }} onClick={() => changePassword()}>
                            <Typography sx={{ color: "#5B4947" }}>
                                Confirm
                            </Typography>
                        </Button>
                    </Box>
                </Box>
            }
        </Box >
    );
};

export default ResetPassword;
