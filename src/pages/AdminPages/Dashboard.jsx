import React from "react";
import Drawers from "../../components/Drawers";
import AdminRoutes from "../../routes/AdminRoutes";


const Dashboard = () => {
    return (
        <>
            <Drawers />
        </>
    );
};

export default Dashboard;
