import { Button, Typography } from "@mui/material";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import UserAdminModal from "../../components/Modals/UserAdminModal";
import AdminTables from "../../components/Tables/AdminTables";
import { UserContext } from '../../Context/UserContext';


const UserPage = () => {
    const [data, setData] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [userData, setUserData] = useState(null)
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setUserData(null)
    };
    const { user } = useContext(UserContext)
    const getUsers = async () => {
        try {
            const response = await axios.get(
                `${process.env.REACT_APP_API}Admin/GetAllUsers`,
                {
                    headers: {
                        Authorization: `Bearer ${user.token}`,
                    },
                }
            );
            const data = response.data;
            setData(data);
        } catch (error) {
            console.log(error);
        }
    };


    const handleTogleStatus = async (userId, email) => {
        try {
            await axios.put(
                `${process.env.REACT_APP_API}Admin/TogleStatusUser?id=${userId}&email=${email}`,
                {},
                {
                    headers: {
                        Authorization: `Bearer ${user.token}`,
                        ContentType: "application/json",
                    },
                }
            );
            getUsers();
        } catch (error) {
            console.log(error);
        }
    };

    const handleOpenEdit = (usr) => {
        handleClickOpen()
        setUserData(usr)
    }
    useEffect(() => {
        getUsers();
    }, []);

    const header = ["Username", "Email", "Role", "Status", "Action"]
    return (
        <>
            <Typography variant="h4" gutterBottom sx={{ marginTop: "16px" }}>
                User Data
            </Typography>
            <Button onClick={() => handleClickOpen()}>Add User</Button>
            <UserAdminModal open={open} handleClose={handleClose} userData={userData} getUsers={getUsers} />

            <AdminTables datas={data} handleOpenEdit={handleOpenEdit} handleTogleStatus={handleTogleStatus} header={header} />

        </>
    );
};

export default UserPage;
