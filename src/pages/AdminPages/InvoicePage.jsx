import { Container, Typography } from "@mui/material";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Tables from "../../components/Tables";
import { UserContext } from "../../Context/UserContext";

const InvoicePage = () => {
    const navigate = useNavigate()
    const baseUrl = process.env.REACT_APP_API
    const { user } = useContext(UserContext);
    const [row, setRow] = useState([])

    const handleFetchData = async () => {
        try {
            const response = await axios.get(`${baseUrl}User/GetAllInvoice`)
            setRow(response?.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        handleFetchData()
    }, []);

    return (
        <Container sx={{ marginTop: "15px" }}>
            <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F', marginTop: '20px' }}>
                Users Invoices
            </Typography>
            <Tables page='invoice' rows={row} heads={['No', 'No.Invoice', 'Date', 'Total Course', 'Total Price', 'Action']} />
        </Container>
    );
};

export default InvoicePage;
