import { Button, Typography } from "@mui/material";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import AdminPaymentModal from "../../components/Modals/AdminPaymentModal";
import AdminTables from "../../components/Tables/AdminTables";
import { UserContext } from '../../Context/UserContext';

const PaymentMethod = () => {
    const { user } = useContext(UserContext)
    const [data, setData] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [paymentData, setPaymentData] = useState(null)
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setPaymentData(null)
    };

    const getPayment = async () => {
        try {
            const response = await axios.get(
                `${process.env.REACT_APP_API}Admin/GetPayments`,
                {
                    headers: {
                        Authorization: `Bearer ${user.token}`,
                    },
                }
            );
            const data = response.data;
            setData(data);
        } catch (error) {
            console.log(error);
        }
    };


    const handleTogleStatus = async (userId) => {
        try {
            await axios.put(
                `${process.env.REACT_APP_API}Admin/TogleStatusPayment?id=${userId}`,
                {},
                {
                    headers: {
                        Authorization: `Bearer ${user.token}`,
                        ContentType: "application/json",
                    },
                }
            );
            getPayment();
        } catch (error) {
            console.log(error);
        }
    };

    const handleOpenEdit = (usr) => {
        handleClickOpen()
        setPaymentData(usr)
    }
    useEffect(() => {
        getPayment();
    }, []);

    const header = ["Logo", "Name", "Status", "Action"]

    return (
        <>
            <Typography variant="h4" gutterBottom sx={{ marginTop: "16px" }}>
                Payment Data
            </Typography>
            <Button onClick={() => handleClickOpen()}>Add Payment</Button>
            <AdminPaymentModal open={open} handleClose={handleClose} paymentData={paymentData} getPayment={getPayment} />

            <AdminTables datas={data} handleOpenEdit={handleOpenEdit} handleTogleStatus={handleTogleStatus} header={header} />
        </>
    );
};

export default PaymentMethod;
